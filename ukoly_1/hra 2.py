import time
import sys

jmeno = input('Napište vaše jméno: ')
print('Vítej', jmeno)
time.sleep(2)


print('Vyber si svoji postavu')
print('1.Válečník')
print('2.Kouzelník')

postava = input('Vyber postavu(1/2):')

if postava == '1':
    print('Jsi válečník')

elif postava == '2':
    print('Jsi kouzelník')

else:
    print('Vybírej pouze čísla z nabídky')

time.sleep(2)
print('Tvoje postava se ocitla v malé vesnici, kterou sužuje zlý drak.')
print('Tvým úkolem je projít skrz Temný les až k jeskyní, ve které žije drak a draka porazit')
time.sleep(2)
print('Hra začíná na okraji Temného lesa')
time.sleep(2)
print('Při průchodu Temného lesa tě přepadl skřet')
print('Útočí na tebe')

print('1.Provést protiútok ')
print('2.Uskočit')

akce = input('Vyber akci(1/2):')

if akce == '1':
    print('Skřet je příliš silný, jedinou ránou tě zabíjí')
    quit()
elif akce == '2' and postava == '1':
    print('Podařilo se ti skřetovi uskočit, ten jen promáchnul, zavrávoral a napíchnul se na větev.')
    print('Od skřeta získáváš meč')
elif akce == '2' and postava == '2':
    print('Podařilo se ti skřetovi uskočit, ten jen promáchnul, zavrávoral a napíchnul se na větev.')
    print('Od skřeta získáváš kouzlo ohnivou kouli')


print('Pokračuješ dál v cestě')
print('Narážíš na most, který stráží trol.')
time.sleep(2)
print('Trol tě pustí dál, jen pokud uhádneš jeho hádanku, pokud však odpovíš špatně, zabije tě')
time.sleep(2)
print('Hádanka zní:')
time.sleep(2)
print('VŠECHNO ŽERE, VŠECHNO SE V NĚM ZTRÁCÍ, STROMY, KVĚTY, ZVÍŘATA I PTÁCI;')
print('HRYŽE KOV I PLÁTY Z OCELE, TVRDÝ KÁMEN NA PRACH SEMELE;')
print('MĚSTA ROZVALÍ A KRÁLE SKOLÍ, VYSOKÁNSKÉ HORY SVRHNE DO ÚDOLÍ.')

time.sleep(3)

odpoved = input('Napiš svoji odpověď:')

if odpoved == 'čas':
    print('Tvoje odpověď je správná, můžeš projít')
    if  postava == '1':
        print('Při průchodu získáváš od trola luk.')
    elif postava == '2':
        print('Při průchodu získáváš od trola kouzlo ledovou kouli.')
else:
    print('Odpověděl jsi špatně, trol tě zabil')
    quit()
time.sleep(2)

print('Konečně jsi se dostal do dračí jeskyně')
time.sleep(2)
print('Drak tu na tebe však již čeká a je připraven  zaútočit')
time.sleep(2)
print('Musíš se rychle rozhodnout, kterou zbraň na draka použiješ')
time.sleep(2)
if postava == '1':
    print('Vyber si zbraň')
    print('1.Meč')
    print('2.Luk')
    zbraň = input('Vyber zbraň(1/2):')
elif postava == '2':
    print('Vyber si kouzlo')
    print('1.Ohnivá koule')
    print('2.Ledová koule')
    kouzlo = input('Vyber kouzlo(1/2):')


time.sleep(2)


if postava == '1' and zbraň == '2':
    print('Povedlo se ti draka střelit do oka, drak padá a mlatí sebou o zem')
    print('Na nic nečekáš, běžíš k němu a pomocí meče mu utínáš hlavu')
    time.sleep(2)
    print('VYHRÁVÁŠ HRU')
elif postava == '1' and zbraň == '1':
    print('jestě než ses k drakovi přiblížil, sežehl tě a umíráš')
elif postava == '2' and kouzlo == '2':
    print ('Správně jsi zvolil ledovou kouli, jelikož se jedná o ohnivého draka')
    print('Ledovou koulí jsi zmrazil draka a ten umírá')
    time.sleep(2)
    print('VYHRÁVÁŠ HRU')

elif postava == '2' and kouzlo == '1':
    print('Ohnivá koule na draka nepůsobí, než stihneš zareagovat, drak tě sežehne')
